%script to convert the STEE json to the clickdrive json format

A = fileread('all_data.json');

idx = find(A=='{');
idx2 = find(A=='}');

%for i=1:length(idx)

for i=1:length(idx)

tmp = textscan(A(idx(i):idx2(i)),'%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s','delimiter','\n');

%process the timestamp component of tmp

tstmp = char(tmp{2});
yrtmp = tstmp(15:18);
mttmp = tstmp(20:21);
dytmp = tstmp(23:24);
hrtmp = tstmp(26:27);
mintmp = tstmp(29:30);
sectmp = tstmp(32:33);
%{
hrtmpd = str2num(hrtmp);
hrtmpd = hrtmpd+8;
if(hrtmpd>24)
hrtmpd = hrtmpd - 24;
end
%}

%construct timedate string

timedate(i,1:2) = hrtmp;
timedate(i,3) = ':';
timedate(i,4:5) = mintmp;
timedate(i,6) = ':';
timedate(i,7:8) = sectmp;

%get timestamp
ts1 = datenum([str2num(yrtmp),str2num(mttmp),str2num(dytmp),str2num(hrtmp),str2num(mintmp),str2num(sectmp)])*24*3600*1000;
ts1970 = datenum(1970,1,1,0,0,0)*24*3600*1000;
ts(i) = round(ts1-ts1970);

%process all other components of tmp
for j=3:length(tmp)-1
tmp2 = textscan(char(tmp{j}),'%s %s','delimiter',':');
numtmp = char(tmp2{2});
%numtmp2 = numtmp(2:end-2);

if(j==3)
long(i) = str2num(numtmp(2:end-2));
end

if(j==4)
lat(i) = str2num(numtmp(2:end-2));
end

if(j==5)
alt(i) = str2num(numtmp(2:end-2));
end

if(j==6)
fixed(i) = str2num(numtmp(2:end-2));
end

if(j==7)
hdop(i) = str2num(numtmp(2:end-2));
end

if(j==8)
pdop(i) = str2num(numtmp(2:end-2));
end

if(j==9)
vdop(i) = str2num(numtmp(2:end-2));
end

if(j==10)
satnum(i) = str2num(numtmp(2:end-2));
end

if(j==11)
heading(i) = str2num(numtmp(2:end-2));
end

if(j==12)
speed(i) = str2num(numtmp(2:end-2));
end

if(j==13)
temp(i) = str2num(numtmp(2:end-2));
end

if(j==14)
ins_acc_x(i) = str2num(numtmp(2:end-2));
end

if(j==15)
ins_acc_y(i) = str2num(numtmp(2:end-2));
end

if(j==16)
ins_acc_z(i) = str2num(numtmp(2:end-2));
end

if(j==17)
ins_gy_x(i) = str2num(numtmp(2:end-2));
end

if(j==18)
ins_gy_y(i) = str2num(numtmp(2:end-2));
end

if(j==19)
ins_gy_z(i) = str2num(numtmp(2:end-1));
end

clear numtmp numtmp2 tmp2 tstmp yrtmp mttmp dytmp hrtmp mintmp sectmp

end
%{
fid = fopen("11mayNewjson.json","a");

% use \" to depict double quotes in string
if(i==1)
megastring = ['[{"c":"1","v":"5733","t":"' num2str(ts(i)) '","d":{"OBDII.TEMP":0,"OBDII.RPM":0.000000,"OBDII.SPEED":'  ... 
num2str(round(speed(i))) ",\"OBDII.VOLTAGE\"" ":0.000," "\"OBDII.TRANSMISSION TEMP\"" ":0," ... 
"\"IMU.FUSION_QPOSE\"" ":[1 0 0 0]," "\"IMU.GYRO\":[" num2str(ins_gy_x(i),6) "," num2str(ins_gy_y(i),6) "," num2str(ins_gy_z(i),6) "]," ...
 "\"IMU.ACCEL\":[" num2str(ins_acc_x(i),6) "," num2str(ins_acc_y(i),6) "," num2str(ins_acc_z(i),6) "],\"IMU.COMPASS\":[0.000000 0.000000 0.0000000]" ...
",\"IMU.TEMP\":0.000000,\"GPS.SURFACE_LOCATION\":[" num2str(lat(i),7) "," num2str(long(i),9) ",0.000000,0.000000],\"GPS.SURFACE_KINETIC\":[" ...
"0.000000,0.000000,0.000000,0.000000]}}" "\n"];
end

if(i>1 && i<length(idx))
megastring = [',{"c":"1","v":"5733","t":"' num2str(ts(i)) '","d":{"OBDII.TEMP":0,"OBDII.RPM":0.000000,"OBDII.SPEED":'  ... 
num2str(round(speed(i))) ",\"OBDII.VOLTAGE\"" ":0.000," "\"OBDII.TRANSMISSION TEMP\"" ":0," ... 
"\"IMU.FUSION_QPOSE\"" ":[1 0 0 0]," "\"IMU.GYRO\":[" num2str(ins_gy_x(i),6) "," num2str(ins_gy_y(i),6) "," num2str(ins_gy_z(i),6) "]," ...
 "\"IMU.ACCEL\":[" num2str(ins_acc_x(i),6) "," num2str(ins_acc_y(i),6) "," num2str(ins_acc_z(i),6) "],\"IMU.COMPASS\":[0.000000 0.000000 0.0000000]" ...
",\"IMU.TEMP\":0.000000,\"GPS.SURFACE_LOCATION\":[" num2str(lat(i),7) "," num2str(long(i),9) ",0.000000,0.000000],\"GPS.SURFACE_KINETIC\":[" ...
"0.000000,0.000000,0.000000,0.000000]}}" "\n"];
end

if(i==length(idx))
megastring = [',{"c":"1","v":"5733","t":"' num2str(ts(i)) '","d":{"OBDII.TEMP":0,"OBDII.RPM":0.000000,"OBDII.SPEED":'  ... 
num2str(round(speed(i))) ",\"OBDII.VOLTAGE\"" ":0.000," "\"OBDII.TRANSMISSION TEMP\"" ":0," ... 
"\"IMU.FUSION_QPOSE\"" ":[1 0 0 0]," "\"IMU.GYRO\":[" num2str(ins_gy_x(i),6) "," num2str(ins_gy_y(i),6) "," num2str(ins_gy_z(i),6) "]," ...
 "\"IMU.ACCEL\":[" num2str(ins_acc_x(i),6) "," num2str(ins_acc_y(i),6) "," num2str(ins_acc_z(i),6) "],\"IMU.COMPASS\":[0.000000 0.000000 0.0000000]" ...
",\"IMU.TEMP\":0.000000,\"GPS.SURFACE_LOCATION\":[" num2str(lat(i),7) "," num2str(long(i),9) ",0.000000,0.000000],\"GPS.SURFACE_KINETIC\":[" ...
"0.000000,0.000000,0.000000,0.000000]}}]" "\n"];
end

fputs(fid,megastring);

%{
fputs(fid,[num2str(ts(i)) ',' timedate(i,:) ",571,GPS.SURFACE_LOCATION,DOUBLE_ARRAY," num2str(lat(i),10) ',' num2str(long(i),10) ",0,0\n"]);
fputs(fid,[num2str(ts(i)) ',' timedate(i,:) ",571,OBDII.SPEED,INT_ARRAY," num2str(round(speed(i))) "\n"]);
fputs(fid,[num2str(ts(i)) ',' timedate(i,:) ",571,OBDII.TEMP,INT_ARRAY," num2str(temp(i)) "\n"]);
fputs(fid,[num2str(ts(i)) ',' timedate(i,:) ",571,IMU.ACCEL,FLOAT_ARRAY," num2str(ins_acc_x(i),10) "," num2str(ins_acc_y(i),10)  "," num2str(ins_acc_z(i),10) "\n"]);
fputs(fid,[num2str(ts(i)) ',' timedate(i,:) ",571,IMU.GYRO,FLOAT_ARRAY," num2str(ins_gy_x(i),10) "," num2str(ins_gy_y(i),10)  "," num2str(ins_gy_z(i),10) "\n"]);
%}
fclose(fid);
clear megastring
%}
end

%end

