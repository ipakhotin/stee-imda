%UKF to estimate position, speed and acceleration from STEE OBU speedometer and INS unit

clear p x x0 x00 X0 X pyy p0 y ym Y K err

%initial state and covariance matrices
p = eye(3);
pinit = eye(3);
xi = [0 0 0]';
pyy = eye(3);

%UKF parameters
n = 3;%dimensionality
alpha = 1;
beta = 2;%optimal for Gaussians
kk=1;
lamda = (alpha^2)*(n+kk) - n;

%sampling period (sec)
interval = 1;

%state and measurement covariances
Q = [1e-1 0 0;0 1e-7 0;0 0 1e-7];
R = [1e-0 0 0;0 1e-6 0;0 0 1.2541e-06];%acc variance taken from a stationary time period from forwardAccel after detrending 400 sec after start of run

for ii=1:500

forwardAccel(ii) = ins_acc_y(ii) - mean(ins_acc_y(1:ii));
if(ii>1)
gpsdist(ii) = gpsdist(ii-1)+111.32*1000*((lat(ii)-lat(ii-1))^2 + (long(ii)-long(ii-1))^2)^0.5;
else
gpsdist(ii) = 0;
end

    if(ii==1)
        X0 = xi(:,ones(1,numel(xi)));
        X = [xi X0+sqrt(n+lamda)*chol(pinit) X0-sqrt(n+lamda)*chol(pinit)];
    else
        xp = x(end,:)';
        X0 = xp(:,ones(1,numel(xp)));
        X = [xp X0+sqrt(n+lamda)*chol(p) X0-sqrt(n+lamda)*chol(p)];
    end
    %generate weights
    w0m = lamda/(n+lamda);
    w1m = 1/(2*(n+lamda));
    w2m = 1/(2*(n+lamda));

    w0c = w0m + (1 - alpha^2 + beta);
    w1c = 1/(2*(n+lamda));
    w2c = 1/(2*(n+lamda));
    
    %propagate sigma points
    Xstar(1,:) = X(1,:) + interval*X(2,:) + 0.5*interval*X(3,:).^2;
    Xstar(2,:) = X(2,:) + interval*X(3,:);
    Xstar(3,:) = X(3,:);
    %define mean + covariance
    x0 = w0m*Xstar(:,1);
    
    for j=2:(2*n + 1)
    x0 = x0 + w1m*Xstar(:,j);
    end

    p0 = w0c*(Xstar(:,1) - x0)*(Xstar(:,1) - x0)' + Q;

    for j=2:(2*n+1)
    p0 = p0 + w1c*(Xstar(:,j)-x0)*(Xstar(:,j)-x0)';
    end
    
    %y = [gpsdist(ii) speed(ii)/3.6 forwardAccel(ii)*9.8]'; 
    y = [NaN speed(ii)/3.6 forwardAccel(ii)*9.8]'; 
    
    Y(1,:) = Xstar(1,:);
    Y(2,:) = Xstar(2,:);
    Y(3,:) = Xstar(3,:);
    
    ym = w0m*Y(:,1);

    for j=2:(2*n + 1)
    ym = ym + w1m*Y(:,j);
    end

    pyy = w0c*(Y(:,1) - ym)*(Y(:,1) - ym)' + R;

    for j=2:(2*n+1)
    pyy = pyy + (w1c*(Y(:,j)-ym)*(Y(:,j)-ym)');
    end
    %Kalman gain
    K = p0*(pyy^(-1));
    
    err(ii,:) = (y-ym);
    
    if(isnan(y(1))==1)
    err(ii,1) = 0;    
    end
    
    if(isnan(y(2))==1)
    err(ii,2) = 0;
    end
    
    if(isnan(y(3))==1)
    err(ii,3) = 0;
    end
    
    x00(ii) = x0(1,1);
    x(ii,:) = x0 + K*(err(ii,:)');
    
    %force magnitude of speed to be always positive
    if(x(ii,2)<0)
    x(ii,2) = 0;
    end

    p = eye(n,n).*(p0 - K*pyy*K');
    
end